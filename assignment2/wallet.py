import os
import sys

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALchemy_DATABASE_URI'] = 'sqlite:////home/sneha/Documents/273_fresh/assignment2/example.db'

db = SQLAlchemy(app)

class Exampletable(db.Model):
    __tablename__ = 'student'
    id =    db.Column('id',db.Integer,primary_key=True)
    #name = db.Column('name',name',db.unicode)