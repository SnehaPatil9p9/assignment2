from flask import Flask, request, jsonify
from config import Config
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import pdb
#from models import models
import jsonify
from datetime import datetime

app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)
ma = Marshmallow(app)
migrate = Migrate(app, db)

from app import models
from models import wallet
from models import txn

class WalletSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('id', 'balance', 'coin_symbol')

wallet_schema = WalletSchema()
wallets_schema = WalletSchema(many=True)

class TxnSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('from_wallet', 'to_wallet', 'amount', 'status', 'time_stamp', 'txn_hash')


txn_schema = TxnSchema()
txns_schema = TxnSchema(many=True)

# endpoint to create new wallet
@app.route("/wallet", methods=["POST"])
def add_wallet():
    print 'request is ' + str(request.json)
    id = request.json['id']
    balance = request.json['balance']
    coin_symbol = request.json['coin_symbol']
    new_wallet = wallet(id, balance, coin_symbol )

    db.session.add(new_wallet)
    db.session.commit()

    #print "new wallet is " + str(new_wallet)
    return jsonify(new_wallet)
    #return 1


# endpoint to show all wallets
@app.route("/wallet", methods=["GET"])
def get_wallet():
    #pdb.set_trace() #debug
    
    all_wallets = wallet.query.all()
    result = wallets_schema.dump(all_wallets)
    print result
    return jsonify(result.data)


# endpoint to get wallet detail by id
@app.route("/wallet/<id>", methods=["GET"])
def wallet_detail(id):
    currwallet = wallet.query.get(id)
    return wallet_schema.jsonify(currwallet)


# endpoint to update wallet
@app.route("/wallet/<id>", methods=["PUT"])
def wallet_update(id):
    wallet = wallet.query.get(id)
    wallet_balance = request.json['balance']
    wallet_coin = request.json['coin_symbol']
    
    wallet.balance = wallet_balance
    wallet.coin_symbol = wallet_coin

    db.session.commit()
    return wallet_schema.jsonify(wallet)


# endpoint to delete wallet
@app.route("/wallet/<id>", methods=["DELETE"])
def wallet_delete(id):
    wallet = wallet.query.get(id)
    db.session.delete(wallet)
    db.session.commit()

    return wallet_schema.jsonify(wallet)

# txns 
@app.route("/txns", methods=["POST"])
def transfer_asset():
    txn_id = request.json['txn_id']
    from_wallet = request.json['from_wallet']
    to_wallet = request.json['from_wallet']
    amount = request.json['amount']
    time_stamp =  datetime(2018, 3, 3, 10, 10, 10)
    txn_hash = request.json['txn_hash']
    status = 'pending'
    #wallet1 = wallet_detail(from_wallet)
    #print 'wallet data is ' + str(wallet1)
    #if (wallet1['balance'] < amount):
    #    status = "rejected"
    
    new_txns = txn(txn_id, from_wallet, to_wallet, amount,  status, time_stamp, txn_hash)

    db.session.add(new_txns)
    db.session.commit()

    #print "new wallet is " + str(new_wallet)
    return jsonify(new_txns)

@app.route("/txns/<hash>", methods=["GET"])   
def get_txn (hash):
    txn_hash = hash
    txn_detail = txn.query.get(hash)
    return txn_schema.jsonify(txn_detail)

@app.route("/txns_all", methods=["GET"])   
def get_txn_all ():
    txn_detail = txn.query.all
    return txn_schema.jsonify(txn_detail)

if __name__ == '__main__':
    app.run(debug=True)

