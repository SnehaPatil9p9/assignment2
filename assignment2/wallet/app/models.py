from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.username) 
    
    def __init__(self, username, email):
        self.username = username
        self.email = email   

class wallet(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    balance = db.Column(db.Float)
    coin_symbol = db.Column(db.String(120), index=True)
    #password_hash = db.Column(db.String(128))
    #def __repr__(self):
    #    return '<wallet {}>'.format(self.id)   
    def __init__(self, id, balance,coin_symbol):
        self.id = id
        self.balance = balance 
        self.coin_symbol = coin_symbol

class txn(db.Model):
    txn_id = db.Column(db.Integer)
    from_wallet = db.Column(db.Integer,  db.ForeignKey('wallet.id'))
    to_wallet = db.Column(db.Integer,  db.ForeignKey('wallet.id'))
    amount = db.Column(db.Float)
    status = db.Column(db.String(80))
    time_stamp = db.Column(db.TIMESTAMP)
    txn_hash = db.Column(db.String(256), index=True, primary_key=True)
    def __repr__(self):
        return '<txn {}>'.format(self.txn_hash)
    def __init__(self, txn_id, from_wallet, to_wallet, amount, status, time_stamp, txn_hash):
        self.txn_id = txn_id
        self.from_wallet = from_wallet
        self.to_wallet = to_wallet 
        self.amount = amount
        self.status = status
        self.time_stamp = time_stamp
        self.txn_hash = txn_hash




